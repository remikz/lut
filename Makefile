.PHONY: help test

all: help

build:
    python setup.py build --build-base=/tmp/build/lut

help:
	@echo make [ test sdist build upload install ]

install:
	python setup.py install
    
sdist:
	python setup.py sdist
    
test:
	PYTHONPATH=$(PWD) python ./test/test_lut.py

upload:
	python setup.py sdist upload -r pypi