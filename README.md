Overview
========
lut is a multi-component interpolating lookup table for nonlinear data mapping.

Features
--------
* Load from file.
* Single or multiple components.
* Piecewise interpolation modes: none, linear, cosine.
* Discrete (table based) or continuous lookup.
* Numpy support optional.
* Commandline script.

Examples
========
	
Python
------

	import lut
	t = lut.load('examples/lut.json')
	t.get(1.5)
	t.get([1, 2, 3])
	
	import numpy
	t.get(numpy.linspace(0,10,100))
	
	spec = {
        "range": [10, 50],
        "size": 100,
        "table": [
            [0, 0],
            [.5, .2],
            [1, 1]
        ]
    }
    t = lut.load(spec)
    t.get(15)
    
    t = lut.Lut(size=100)
    t.add(0, 255, 255, 0)
    t.add(1, 0, 128, 128)
    t.build()
    t.range = (-10, 400)
    t.get(300)

Commandline
-----------
    
Lookup single value.

    $ lut -t lut.json -r 2,100 1.23

Lookup many values in file.

    $ echo 1 2,3 > vals && echo 4 >> vals
    $ lut -t lut.json -m cosine vals


Testing
=======

    make test

Installation
============

From PyPi.

    pip install lut
    
From tarball.

    python setup.py install

License
=======

lut  Copyright (C) 2016  Remik Ziemlinski

This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it under the 
conditions of the GPLv3 license.
