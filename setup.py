from distutils.core import setup

setup(
    name='lut',
    packages=['lut'],
    version='1.0.1',
    description='Multi-component interpolation lookup table.',
    author='Remik Ziemlinski',
    author_email='first.last@gmail.com',
    license='GPLv3',
    scripts=['scripts/lut'],
    url='https://www.github.com/rsmz/lut',
    download_url='https://github.com/rsmz/lut/archive/lut-1.0.1.tar.gz',
)
